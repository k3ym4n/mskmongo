package org.dvd.mongoMsk.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by k3ym4n on 06/03/2016.
 */
public class Util {

    public static String formatFecha(Date fecha) {
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

        return format.format(fecha);
    }

    public static Date parseFecha(String fecha) throws ParseException {
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

        return format.parse(fecha);
    }
}
