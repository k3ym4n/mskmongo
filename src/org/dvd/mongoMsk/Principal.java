package org.dvd.mongoMsk;

import org.dvd.mongoMsk.face.Ventana;
import org.dvd.mongoMsk.gui.VentanaController;
import org.dvd.mongoMsk.gui.VentanaModel;

import java.text.ParseException;

/**
 * Created by k3ym4n on 06/03/2016.
 */
public class Principal {

    public static  void main (String args[]){
        VentanaModel model = new VentanaModel();
        Ventana view = new Ventana();
        VentanaController controller = new VentanaController(view,model,view.getBotonera());


    }
}
