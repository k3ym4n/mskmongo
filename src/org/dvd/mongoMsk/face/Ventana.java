package org.dvd.mongoMsk.face;

import com.toedter.calendar.JDateChooser;

import javax.swing.*;

/**
 * Created by k3ym4n on 06/03/2016.
 */
public class Ventana {
    public  JTabbedPane PanelPrincipal;
    public  JPanel TODO;
    public JComboBox cbdiscogrupo;
    public JComboBox cbcanciondisco;
    public JTable tablaCanciones;
    public JTable tablaDiscos;
    public JTable tablaGrupos;
    public JTextField tfgruponombre;
    public JTextField tfgruponacionalidad;
    public JTextField tfdiscotitulo;
    public JTextField intdisconumcanciones;
    public JTextField floatdiscoprecio;
    public JTextField tfcanciontitulo;
    public JTextField intcancionduracion;
    public JTextField tfcanciongenero;
    public JDateChooser dategrupofecha;
    public Botonera Botonera;
    public JTextField tfbuscarGrupo;
    public JButton btBuscarGrupo;
    public JTextField tfbuscarDisco;
    public JButton btbuscarDisco;
    public JTextField tfbuscarCancion;
    public JButton btbuscarCancion;
    public JList JlistaCanciones;
    public JList JlistaGrupos;
    public JList JlistaDiscos;

    public Ventana() {
        JFrame frame = new JFrame("MONGODB");
        frame.setContentPane(TODO);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public Botonera getBotonera() {
        return Botonera;
    }

    public void setBotonera(Botonera botonera) {
        Botonera = botonera;
    }
}
