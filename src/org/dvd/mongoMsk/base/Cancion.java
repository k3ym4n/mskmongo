package org.dvd.mongoMsk.base;

import org.bson.types.ObjectId;

/**
 * Created by k3ym4n on 06/03/2016.
 */
public class Cancion {

    private ObjectId id;
    private String titulo;
    private String duracion;
    private String genero;
    private String disco;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getDisco() {
        return disco;
    }

    public void setDisco(String disco) {
        this.disco = disco;
    }

    @Override
    public String toString() {
        return titulo;
    }
}
