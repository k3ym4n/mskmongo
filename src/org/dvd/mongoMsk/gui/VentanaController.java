package org.dvd.mongoMsk.gui;

import org.dvd.mongoMsk.base.Cancion;
import org.dvd.mongoMsk.base.Disco;
import org.dvd.mongoMsk.base.Grupo;
import org.dvd.mongoMsk.face.Botonera;
import org.dvd.mongoMsk.face.Ventana;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by k3ym4n on 06/03/2016.
 */
public class VentanaController implements ActionListener{

    private VentanaModel model;
    private Ventana view;
    private Botonera botonera;

    private DefaultListModel dlmGrupo,dlmDisco,dlmCancion;

    private Grupo grupo;
    private Disco disco;
    private Cancion cancion;

    boolean nuevo = true;


    public VentanaController(Ventana view, VentanaModel model, Botonera botonera)  {
        this.view = view;
        this.model = model;
        this.botonera = botonera;

        listeners();

        modeloListas();
        listarGrupos();
        listarDiscos();
        listarCanciones();
        rellenarComboGrupos();
        rellenarComboDiscos();

        view.PanelPrincipal.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int pestana = view.PanelPrincipal.getSelectedIndex();
                switch (pestana) {
                    case 0:
                        rellenarComboGrupos();
                        break;
                    case 1:
                        rellenarComboDiscos();

                        break;
                    case 2:

                        break;
                }
            }
        });
        view.JlistaGrupos.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String nombre = view.JlistaGrupos.getSelectedValue().toString();
                Grupo grupo = model.grupoAmodificar(nombre);
                view.tfgruponombre.setText(grupo.getNombre());
                view.dategrupofecha.setDate(grupo.getFecha());
                view.tfgruponacionalidad.setText(grupo.getNacionalidad());
            }
        });

        view.JlistaDiscos.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String titulo = view.JlistaDiscos.getSelectedValue().toString();
                Disco disco = model.discoAmodificar(titulo);
                view.tfdiscotitulo.setText(disco.getTitulo());
                view.intdisconumcanciones.setText(disco.getNumCanciones());
                view.floatdiscoprecio.setText(disco.getPrecio());
                view.cbdiscogrupo.setSelectedItem(disco.getGrupo());
            }
        });

        view.JlistaCanciones.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String titulo = view.JlistaCanciones.getSelectedValue().toString();
                Cancion cancion = model.cancionAmodificar(titulo);
                view.tfcanciontitulo.setText(cancion.getTitulo());
                view.intcancionduracion.setText(cancion.getDuracion());
                view.tfcanciongenero.setText(cancion.getGenero());
                view.cbcanciondisco.setSelectedItem(cancion.getDisco());
            }
        });
    /*    view.tablaGrupos.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String nombre = (String) view.tablaGrupos.getValueAt(view.tablaGrupos.getSelectedRow(), 0);
                Grupo grupo = model.grupoAmodificar(nombre);
                view.tfgruponombre.setText(grupo.getNombre());
                view.dategrupofecha.setDate(grupo.getFecha());
                view.tfgruponacionalidad.setText(grupo.getNacionalidad());
            }
        });

        view.tablaDiscos.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String titulo = (String) view.tablaDiscos.getValueAt(view.tablaDiscos.getSelectedRow(), 0);
                Disco disco = model.discoAmodificar(titulo);
                view.tfdiscotitulo.setText(disco.getTitulo());
                view.intdisconumcanciones.setText(disco.getNumCanciones());
                view.floatdiscoprecio.setText(disco.getPrecio());
                view.cbdiscogrupo.setSelectedItem(disco.getGrupo());
            }
        });

        view.tablaCanciones.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String titulo = (String) view.tablaCanciones.getValueAt(view.tablaCanciones.getSelectedRow(), 0);
                Cancion cancion = model.cancionAmodificar(titulo);
                view.tfcanciontitulo.setText(cancion.getTitulo());
                view.intcancionduracion.setText(cancion.getDuracion());
                view.tfcanciongenero.setText(cancion.getGenero());
                view.cbcanciondisco.setSelectedItem(cancion.getDisco());
            }
        });*/
    }

    private void listeners(){
        botonera.btnuevo.addActionListener(this);
        botonera.btcancelar.addActionListener(this);
        botonera.bteliminar.addActionListener(this);
        botonera.btguardar.addActionListener(this);
        botonera.btmodificar.addActionListener(this);

        view.btBuscarGrupo.addActionListener(this);
        view.btbuscarDisco.addActionListener(this);
        view.btbuscarCancion.addActionListener(this);
    }

    private void modeloListas(){
        dlmGrupo = new DefaultListModel();
        dlmDisco = new DefaultListModel();
        dlmCancion = new DefaultListModel();
    }

    private void listarGrupos(){
        try {
            ArrayList<Grupo> listaGrupos = (ArrayList<Grupo>) model.getGrupos();
            view.JlistaGrupos.setModel(dlmGrupo);
            dlmGrupo.removeAllElements();
            for(Grupo grupo : listaGrupos){
                dlmGrupo.addElement(grupo);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    private void listarDiscos(){
        try {
            ArrayList<Disco> listaDiscos = (ArrayList<Disco>) model.getDiscos();
            view.JlistaDiscos.setModel(dlmDisco);
            dlmDisco.removeAllElements();
            for(Disco disco : listaDiscos){
                dlmDisco.addElement(disco);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    private void listarCanciones(){
        try {
            ArrayList<Cancion> listaCanciones = (ArrayList<Cancion>) model.getCanciones();
            view.JlistaCanciones.setModel(dlmCancion);
            dlmCancion.removeAllElements();
            for(Cancion cancion : listaCanciones){
                dlmCancion.addElement(cancion);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

 /*   private void modeloTablas(){
        dtmGrupo = new DefaultTableModel();
        dtmGrupo.addColumn("Nombre");
        dtmGrupo.addColumn("Fecha");
        dtmGrupo.addColumn("Nacionalidad");
        view.tablaGrupos.setModel(dtmGrupo);

        dtmDisco = new DefaultTableModel();
        dtmDisco.addColumn("Titulo");
        dtmDisco.addColumn("NēCanciones");
        dtmDisco.addColumn("Precio");
        dtmDisco.addColumn("Grupo");
        view.tablaDiscos.setModel(dtmDisco);

        dtmCancion = new DefaultTableModel();
        dtmCancion.addColumn("Titulo");
        dtmCancion.addColumn("Duracion");
        dtmCancion.addColumn("Genero");
        dtmCancion.addColumn("Disco");
        view.tablaCanciones.setModel(dtmCancion);
    }*/


    private void edicionCampos(boolean D){
        int pestana = view.PanelPrincipal.getSelectedIndex();
        switch (pestana){
            case 0:
                view.tfgruponombre.setEditable(D);
                view.dategrupofecha.setEnabled(D);
                view.tfgruponacionalidad.setEditable(D);
                break;
            case 1:
                view.tfdiscotitulo.setEditable(D);
                view.intdisconumcanciones.setEditable(D);
                view.floatdiscoprecio.setEditable(D);
                view.cbdiscogrupo.setEnabled(D);
                break;
            case 2:
                view.tfcanciontitulo.setEnabled(D);
                view.intcancionduracion.setEnabled(D);
                view.tfcanciongenero.setEnabled(D);
                view.cbcanciondisco.setEnabled(D);
                break;
        }
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        int pestana = view.PanelPrincipal.getSelectedIndex();
        if (e.getSource() == botonera.btnuevo) {
            switch (pestana) {
                case 0:
                    nuevo=true;
                    edicionCampos(true);
                    break;
                case 1:
                    nuevo=true;
                    edicionCampos(true);
                    break;
                case 2:
                    nuevo=true;
                    edicionCampos(true);
                    break;
            }
        } else if (e.getSource() == botonera.btguardar) {
            switch (pestana) {

                case 0:

                    if (nuevo) {
                        grupo = new Grupo();
                    } else {
                        String nombre = view.JlistaGrupos.getSelectedValue().toString();
                        grupo = model.grupoAmodificar(nombre);

                    }

                    grupo.setNombre(view.tfgruponombre.getText());
                    grupo.setFecha(view.dategrupofecha.getDate());
                    grupo.setNacionalidad(view.tfgruponacionalidad.getText());

                    if (nuevo) {
                        model.guardarGrupo(grupo);
                    } else {
                        model.modificarGrupo(grupo);
                    }

                    rellenarComboGrupos();
                    listarGrupos();
                    break;
                case 1:

                    if (nuevo) {
                        disco = new Disco();
                    } else {
                        String tituloD = view.JlistaDiscos.getSelectedValue().toString();
                        disco = model.discoAmodificar(tituloD);
                    }
                    disco.setTitulo(view.tfdiscotitulo.getText());
                    disco.setNumCanciones(view.intdisconumcanciones.getText());
                    disco.setPrecio(view.floatdiscoprecio.getText());
                    disco.setGrupo(view.cbdiscogrupo.getSelectedItem().toString());
                    if (nuevo) {
                        model.guardarDisco(disco);
                    } else {
                        model.modificarDisco(disco);
                    }

                    rellenarComboDiscos();
                    listarDiscos();
                    break;
                case 2:
                    if (nuevo) {
                        cancion = new Cancion();
                    } else {
                        String tituloC = view.JlistaCanciones.getSelectedValue().toString();
                        cancion = model.cancionAmodificar(tituloC);
                    }

                    cancion.setTitulo(view.tfcanciontitulo.getText());
                    cancion.setDuracion(view.intcancionduracion.getText());
                    cancion.setGenero(view.tfcanciongenero.getText());
                    cancion.setDisco(view.cbcanciondisco.getSelectedItem().toString());

                    if (nuevo) {
                        model.guardarCancion(cancion);
                    } else {
                        model.modificarCancion(cancion);
                    }
                    listarCanciones();
                    break;
            }
        } else if (e.getSource() == botonera.btmodificar) {
            switch (pestana) {
                case 0:
                    nuevo = false;

                    break;
                case 1:
                    nuevo = false;
                    view.cbdiscogrupo.setSelectedIndex(0);
                    break;
                case 2:
                    nuevo = false;
                    view.cbcanciondisco.setSelectedIndex(0);
                    break;
            }
        } else if (e.getSource() == botonera.bteliminar) {
            switch (pestana) {
                case 0:
                    String nombre =  view.JlistaGrupos.getSelectedValue().toString();
                    grupo = model.grupoAmodificar(nombre);
                    model.eliminarGrupo(grupo);

                    rellenarComboGrupos();
                    listarGrupos();
                    break;
                case 1:
                    String tituloD = view.JlistaDiscos.getSelectedValue().toString();
                    disco = model.discoAmodificar(tituloD);
                    model.eliminarDisco(disco);

                    rellenarComboDiscos();
                    listarDiscos();
                    break;
                case 2:
                    String tituloC = view.JlistaCanciones.getSelectedValue().toString();
                    cancion = model.cancionAmodificar(tituloC);
                    model.eliminarCancion(cancion);
                    listarCanciones();
                    break;
            }
        } else if (e.getSource() == botonera.btcancelar) {
            switch (pestana) {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    break;
            }
        } else if (e.getSource() == view.btBuscarGrupo) {
            if(view.tfbuscarGrupo.getText().equalsIgnoreCase("")){
                listarGrupos();
                return;
            }else{
                String nombre = view.tfbuscarGrupo.getText();
                buscarGrupo(nombre);
                
            }



        }
    }
   /* private void listarTablas() throws ParseException {
        int pestana = view.PanelPrincipal.getSelectedIndex();
        switch (pestana) {
            case 0:
               List<Grupo> grupos = model.getGrupos();
                dtmGrupo.setNumRows(0);
                for(Grupo grupo : grupos){
                    String nombre = grupo.getNombre();
                    Date fecha = grupo.getFecha();
                    String nacionalidad = grupo.getNacionalidad();
                    Object [] filaG = new Object[]{nombre,fecha,nacionalidad};
                    dtmGrupo.addRow(filaG);
                }
                break;
            case 1:
                List<Disco> discos = model.getDiscos();
                dtmDisco.setNumRows(0);
                for(Disco disco : discos){
                    String titulo = disco.getTitulo();
                    String numCanciones = disco.getNumCanciones();
                    String precio = disco.getPrecio();
                    String grupo = disco.getGrupo();
                    Object [] filaD = new Object[]{titulo,numCanciones,precio,grupo};
                    dtmDisco.addRow(filaD);
                }
                break;
            case 2:
                List<Cancion> canciones = model.getCanciones();
                dtmCancion.setNumRows(0);
                for(Cancion cancion : canciones){
                    String titulo = cancion.getTitulo();
                    String duracion = cancion.getDuracion();
                    String genero = cancion.getGenero();
                    String disco = cancion.getDisco();
                    Object [] filaC = new Object[]{titulo,duracion,genero,disco};
                    dtmCancion.addRow(filaC);
                }
                break;
        }
    }*/

    private void rellenarComboGrupos(){
        List<Grupo> grupos = null;
        try {
            grupos = model.getGrupos();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        view.cbdiscogrupo.removeAllItems();

        for (Grupo grupo : grupos) {

            view.cbdiscogrupo.addItem(grupo.getNombre());
        }

    }

    private void rellenarComboDiscos(){
        List<Disco> discos = null;
        try {
            discos = model.getDiscos();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        view.cbcanciondisco.removeAllItems();

        for (Disco disco : discos) {

            view.cbcanciondisco.addItem(disco.getTitulo());
        }
    }


    private void buscarGrupo(String txt)  {
        List<Grupo> BGrupo = null;
        try {
            BGrupo = model.getGrupos();
            List<Grupo> listaDGrupos = new ArrayList<>();

            listaDGrupos = model.getBuscarGrupo(view.tfbuscarGrupo.getText());
            dlmGrupo.removeAllElements();
            for(Grupo grupo : listaDGrupos){
                dlmGrupo.addElement(grupo);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void buscarDisco(String txt)  {

    }

  /*  private void buscarCancion(String txt)  {
        List<Grupo> BCancion = null;
        try {
            BCancion = model.getGrupos();
            List<Cancion> listaDCanciones = new ArrayList<>();

            listaDCanciones = model.getBuscarCancion(view.tfbuscarCancion.getText());
            dlmCancion.removeAllElements();
            for(Cancion cancion : listaDCanciones){
                dlmCancion.addElement(cancion);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }*/

}
