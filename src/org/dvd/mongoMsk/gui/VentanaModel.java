package org.dvd.mongoMsk.gui;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.dvd.mongoMsk.base.Cancion;
import org.dvd.mongoMsk.base.Disco;
import org.dvd.mongoMsk.base.Grupo;
import org.dvd.mongoMsk.util.Util;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by k3ym4n on 06/03/2016.
 */
public class VentanaModel {

    private MongoClient mongoClient;
    private MongoDatabase db;



    public VentanaModel(){
        conectar();
    }

    public void conectar(){
        mongoClient = new MongoClient();
        db = mongoClient.getDatabase("MSKMongoDB");
    }

    public void guardarGrupo(Grupo grupo){
        Document doc = new Document()
                .append("nombre", grupo.getNombre())
                .append("fecha",Util.formatFecha(grupo.getFecha()))
                .append("nacionalidad",grupo.getNacionalidad());
        db.getCollection("Grupo").insertOne(doc);
    }


    public void eliminarGrupo(Grupo grupo){
        db.getCollection("Grupo").deleteOne(new Document("_id", grupo.getId()));
    }


    public void modificarGrupo(Grupo grupo){
        db.getCollection("Grupo").replaceOne(new Document("_id", grupo.getId()),
                new Document()
                        .append("nombre", grupo.getNombre())
                        .append("fecha", Util.formatFecha(grupo.getFecha()))
                        .append("nacionalidad", grupo.getNacionalidad()));
    }

    public Grupo getGrupo(Document doc)throws ParseException{
        Grupo grupo = new Grupo();
        grupo.setId(doc.getObjectId("_id"));
        grupo.setNombre(doc.getString("nombre"));
        grupo.setFecha(Util.parseFecha(doc.getString("fecha")));
        grupo.setNacionalidad(doc.getString("nacionalidad"));
        return grupo;
    }


    public List<Grupo> getGrupos() throws ParseException {
        FindIterable<Document>findIterable = db.getCollection("Grupo").find();
        return getListaGrupos(findIterable);
    }


    public List<Grupo> getListaGrupos(FindIterable<Document> findIterable)throws  ParseException {
        List<Grupo> grupo = new ArrayList<>();
        Grupo g = null;
        Iterator<Document> iter = findIterable.iterator();
        while(iter.hasNext()){
            Document doc = iter.next();
            g = new Grupo();
            g.setId(doc.getObjectId("_id"));
            g.setNombre(doc.getString("nombre"));
            g.setFecha(Util.parseFecha(doc.getString("fecha")));
            g.setNacionalidad(doc.getString("nacionalidad"));
            grupo.add(g);
        }
        return grupo;
    }


    public Grupo grupoAmodificar(String nombre){
        FindIterable<Document> document=db.getCollection("Grupo").find(new Document("nombre", nombre));
        Iterator<Document> iterator=document.iterator();
        Document doc=iterator.next();
        try {
            return getGrupo(doc);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }




    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void guardarDisco(Disco disco){
        Document doc = new Document()
                .append("titulo",disco.getTitulo())
                .append("numeroCanciones", String.valueOf(disco.getNumCanciones()))
                .append("precio", String.valueOf(disco.getPrecio()))
                .append("grupo",disco.getGrupo());
        db.getCollection("Disco").insertOne(doc);
    }

    public void eliminarDisco(Disco disco){
        db.getCollection("Disco").deleteOne(new Document("_id", disco.getId()));
    }


    public void modificarDisco(Disco disco){
        db.getCollection("Disco").replaceOne(new Document("_id", disco.getId()),
                new Document()
                        .append("titulo", disco.getTitulo())
                        .append("numeroCanciones", String.valueOf(disco.getNumCanciones()))
                        .append("precio", String.valueOf(disco.getPrecio()))
                        .append("grupo", disco.getGrupo()));
    }




    public Disco getDisco(Document doc)throws ParseException{
        Disco disco = new Disco();
        disco.setId(doc.getObjectId("_id"));
        disco.setTitulo(doc.getString("titulo"));
        disco.setNumCanciones(doc.getString("numeroCanciones"));
        disco.setPrecio(doc.getString("precio"));
        disco.setGrupo(doc.getString("grupo"));
        return disco;
    }

    public Disco discoAmodificar(String titulo) {
        FindIterable<Document> document=db.getCollection("Disco").find(new Document("titulo", titulo));
        Iterator<Document> iterator=document.iterator();
        Document doc=iterator.next();
        try {
            return getDisco(doc);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    public List<Disco> getDiscos() throws ParseException {
        FindIterable<Document>findIterable = db.getCollection("Disco").find();
        return getListaDiscos(findIterable);
    }

    private List<Disco> getListaDiscos(FindIterable<Document> findIterable) {
        List<Disco> disco = new ArrayList<>();
        Disco d = null;
        Iterator<Document> iter = findIterable.iterator();
        while (iter.hasNext()) {
            Document doc = iter.next();
            d = new Disco();
            d.setId(doc.getObjectId("_id"));
            d.setTitulo(doc.getString("titulo"));
            d.setNumCanciones(doc.getString("numeroCanciones"));
            d.setPrecio(doc.getString("precio"));
            d.setGrupo(doc.getString("grupo"));
            disco.add(d);
        }
        return disco;
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public void guardarCancion(Cancion cancion){
        Document doc = new Document()
                .append("titulo",cancion.getTitulo())
                .append("duracion",cancion.getDuracion())
                .append("genero", cancion.getGenero())
                .append("disco", cancion.getDisco());
        db.getCollection("Cancion").insertOne(doc);
    }
    public void eliminarCancion(Cancion cancion){
        db.getCollection("Cancion").deleteOne(new Document("_id", cancion.getId()));
    }


    public void modificarCancion(Cancion cancion){
        db.getCollection("Cancion").replaceOne(new Document("_id", cancion.getId()),
                new Document()
                        .append("titulo", cancion.getTitulo())
                        .append("duracion", cancion.getDuracion())
                        .append("genero", cancion.getGenero())
                        .append("disco", cancion.getDisco()));
    }

    public Cancion getCancion(Document doc)throws ParseException{
        Cancion cancion = new Cancion();
        cancion.setId(doc.getObjectId("_id"));
        cancion.setTitulo(doc.getString("titulo"));
        cancion.setDuracion(doc.getString("duracion"));
        cancion.setGenero(doc.getString("genero"));
        cancion.setDisco(doc.getString("disco"));
        return cancion;
    }

    public Cancion cancionAmodificar(String titulo) {
        FindIterable<Document> document=db.getCollection("Cancion").find(new Document("titulo", titulo));
        Iterator<Document> iterator=document.iterator();
        Document doc=iterator.next();
        try {
            return getCancion(doc);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    public List<Cancion> getCanciones() throws ParseException {
        FindIterable<Document>findIterable = db.getCollection("Cancion").find();
        return getListaCanciones(findIterable);
    }

    private List<Cancion> getListaCanciones(FindIterable<Document> findIterable) {
        List<Cancion> cancion = new ArrayList<>();
        Cancion c = null;
        Iterator<Document> iter = findIterable.iterator();
        while (iter.hasNext()) {
            Document doc = iter.next();
            c = new Cancion();
            c.setId(doc.getObjectId("_id"));
            c.setTitulo(doc.getString("titulo"));
            c.setDuracion(doc.getString("duracion"));
            c.setGenero(doc.getString("genero"));
            c.setDisco(doc.getString("disco"));
            cancion.add(c);
        }
        return cancion;
    }

///////////////////////////////////////////////////////////////////////////////////////////////
    public List<Grupo> getBuscarGrupo(String txt) throws ParseException {
        Document document = new Document("$or", Arrays.asList(
                new Document("nombre",txt),
                new Document("fecha ",txt),
                new Document("nacionalidad",txt)
        ));
        FindIterable findIterable = db.getCollection("Grupo")
                .find(document)
                .sort(new Document("nombre", 1));
        return getListaGrupos(findIterable);
    }

    public List<Grupo> getBuscarDisco(String txt) throws ParseException {
        Document document = new Document("$or", Arrays.asList(
                new Document("titulo",txt),
                new Document("numCanciones ",txt),
                new Document("precio",txt),
                new Document("grupo",txt)
        ));
        FindIterable findIterable = db.getCollection("Disco")
                .find(document)
                .sort(new Document("titulo",1));
        return getListaGrupos(findIterable);
    }

    public List<Grupo> getBuscarCancion(String txt) throws ParseException {
        Document document = new Document("$or", Arrays.asList(
                new Document("titulo",txt),
                new Document("duracion ",txt),
                new Document("genero",txt),
                new Document("disco",txt)
        ));
        FindIterable findIterable = db.getCollection("Cancion")
                .find(document)
                .sort(new Document("titulo",1));
        return getListaGrupos(findIterable);
    }
}
